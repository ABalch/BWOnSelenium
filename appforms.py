from flask_wtf import FlaskForm
from wtforms import TextField, SubmitField
from wtforms.validators import DataRequired

class Supp_field(FlaskForm):
    supp_id = TextField('SupplierID', validators=[DataRequired()])
    submit = SubmitField('Submit')