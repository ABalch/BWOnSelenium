"""
Routes and views for the flask application.
"""
import zeep
from datetime import datetime
from flask import render_template, request
from webserv import app
from .agresso_websv import Agresso
from .appforms import Supp_field
import json
app.secret_key = 'development key'

#####################################################################################################################
# the login credentials are taken from a config.json file which should be held outside this folder, the format of the
# json file can be found in the config.json file within this folder.

with open ('C:\\TEST\\config.json') as json_data_file:
    data = json.load(json_data_file)

####################################################################################################################

@app.route('/')
@app.route('/home')
def home():
    """Renders the home page."""
    return render_template(
        'index.html',
        title='Home Page',
        year=datetime.now().year,
        form = Supp_field()
    )

@app.route('/contact')
def contact():
    """Renders the contact page."""
    return render_template(
        'contact.html',
        title='Contact',
        year=datetime.now().year,
        message='Your contact page.'
    )

@app.route('/about')
def about():
    """Renders the about page."""
    return render_template(
        'about.html',
        title='About',
        year=datetime.now().year,
        message='Your application description page.'
    )

@app.route('/result', methods=['POST'])
def result():
    """Returns the result page for the web service query"""
    return render_template(
        'result.html',
        sup_submitted = request.form['supp_id'],
        agr = Agresso(data["creds"]["login"],data["creds"]["client"],data["creds"]["passwd"],request.form['supp_id']).webservice()
        )
    