Make sure the web service is functioning correctly by using the free tool SOAP UI which can be downloaded here https://www.soapui.org/

I've used VSCode as my code editor.

Set up the Python virtual environment

Set up your config.json file the path of which is contained in views.py a template of this file is config.json
this file holds the login credentials for the web service user who is accessing the web service as well as the url of the webservice

Install the relevant python packages needed to run the application.  See the requirements.txt file for details.

Execute the Agresso web service package by calling python -m flask run from the command line

You may have to set the FLASK_APP environment variable if using Windows to do this run $env:FLASK_APP = "app.py" from the command line.

This is a very basic web app that uses the If Supplier Exists web service to return True or False if the Supplier is in the Agresso database
This is just a proof of concept test and needs expanding and tidying up before developed more.