#this python code works and connects to supplier web service on aguat just populate the login credential variables then run

class Agresso:
    def __init__ (self, user_id, cli, pw, sup_id):
        self.user_id = user_id
        self.cli = cli
        self.pw = pw
        self.sup_id = sup_id

    def webservice(self):
        import zeep
        import json
        with open ('C:\\TEST\\WebService\\config.json') as json_data_file:
            data = json.load(json_data_file)
        WSCredentials = {'Username': self.user_id , 'Client': self.cli, 'Password': self.pw}
        wsdl = data["base_url"]["url"]
        client = zeep.Client(wsdl=wsdl)

        return(client.service.SupplierExists('UB', self.sup_id, WSCredentials))